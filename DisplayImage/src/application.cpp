#include "application.h"
#include <iostream>
using namespace std;

const int WINDOW_HEIGHT = 720, WINDOW_WIDTH = 1280;
const int IMAGE_BOARDER_SIZE = 40;

Application::Application()
{
	m_sdlSurface = nullptr;
	m_sdlWindow = nullptr;
}

bool Application::initialize()
{
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
		return false;
	}

	//Create SDL Window
	m_sdlWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
	if (m_sdlWindow == NULL)
	{
		cout << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
		return false;
	}

	//Get Window Surface
	m_sdlSurface = SDL_GetWindowSurface(m_sdlWindow);
	SDL_FillRect(m_sdlSurface, nullptr, SDL_MapRGB(m_sdlSurface->format, 0x00, 0x00, 0x00));

	return true;

}

void Application::run()
{
	bool quit = false;
	while (!quit)
	{
		//Check for quit event
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT) quit = true;
		}

		//Update game state

		//Render game elements
		SDL_Rect drawRect; drawRect.x = IMAGE_BOARDER_SIZE; drawRect.y = IMAGE_BOARDER_SIZE; drawRect.w = WINDOW_WIDTH - 2 * IMAGE_BOARDER_SIZE; drawRect.h = WINDOW_HEIGHT - 2 * IMAGE_BOARDER_SIZE;;
		SDL_FillRect(m_sdlSurface, &drawRect, SDL_MapRGB(m_sdlSurface->format, 0xff, 0xff, 0xff));

		//
		SDL_UpdateWindowSurface(m_sdlWindow);
	}
}

void Application::uninitialize()
{
	//Destroy Window
	SDL_DestroyWindow(m_sdlWindow);

	//
	SDL_Quit();
}