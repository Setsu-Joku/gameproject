
#include "application.h"

int main(int argc, char* argv[]) {

	Application game;

	if (!game.initialize())
		return 1;
	game.run();
	game.uninitialize();

	return 0;

}
