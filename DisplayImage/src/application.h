#pragma once

#include <SDL.h>

/*
	This class encapsulates the SDL application - it's initialization, it's update loop, member variables ans uninitialization
*/

class Application
{

public:

	Application();
	bool initialize();
	void run();
	void uninitialize();

private:
	SDL_Window* m_sdlWindow;
	SDL_Surface* m_sdlSurface;
};