
#include <SDL.h>
#include <iostream>

using namespace std;

//	Screen size
const int WIDTH = 640;
const int HEIGHT = 480;

int main(int argc, char* args[])
{
	SDL_Window* window = NULL;				//	A pointer to the SDL window we'll be creating...
	SDL_Surface* surface = NULL;			//	The drawing surface of the window

	//	Initializing SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
		return 1;
	}
	else
	{
		//	If SDL init succeeded, then create the a window using SDL 
		window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			cout << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
		}
		else
		{	//	If window creation succeeded...
			//	1. Get window's draw surface
			surface = SDL_GetWindowSurface(window);

			//	2. Fill the surface white with solid black 
			SDL_Rect drawRect; drawRect.x = 40; drawRect.y = 40; drawRect.w = 560; drawRect.h = 400;
			SDL_FillRect(surface, &drawRect, SDL_MapRGB(surface->format, 0xff, 0xff, 0xff));

			//	3. Update the surface
			SDL_UpdateWindowSurface(window);

			//	4. Wait till we receive SDL_QUIT event and then proceed
			SDL_Event event;
			bool shouldQuit = false;
			while (!shouldQuit) {
				//	Check all the events in the queue
				while (SDL_PollEvent(&event)) { 
					//	If we see a SDL_QUIT event, exit immediately
					if (event.type == SDL_QUIT) shouldQuit = true; 
				}
			}
		}
	}

	//Destroy window
	SDL_DestroyWindow(window);

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}
